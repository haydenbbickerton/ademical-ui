// Grab Our Configuration
window.appConfig = require('../../../config');

// Import Vue Stuff
window.Vue = require('vue');
window.VueRouter = require('vue-router');
window.VueAsyncData = require('vue-async-data');

// Get all of our dependencies
require('./plugins/init.js');
require('./mixins/init.js');
require('./filters/init.js');
require('./components/init.js');

// Import the actual routes, aliases, ...
import { configRouter } from './routes';

const router = new VueRouter(
{
    hashbang: false,
    history: true,
    saveScrollPosition: true,
    linkActiveClass: 'active',
});

// Inject the routes into the VueRouter object
configRouter(router);

Vue.config.debug = true;

// Configure the vue-resource client
Vue.use(require('vue-resource')); 
Vue.http.options = appConfig.http.options;
Vue.http.headers.common = appConfig.http.headers.common;
Vue.http.interceptors.push(require('./interceptors/jwt'));
Vue.http.interceptors.push(require('./interceptors/data'));


/*
	Mixins
 */
Vue.use(VueAsyncData);
Vue.use(appColors);

// Bootstrap the app
const App = Vue.extend(require('./app.vue'));
window.App = App;


// Aaaaaaand we're off!
window.router = router;
router.start(App, 'ademical');
