(function (define) {
  'use strict'

  define(function (require) {

    /**
     * Authenticates the request using JWT Authentication
     *
     * @param {Client} [client] client to wrap
     * @param {Object} config
     *
     * @returns {Client}
     */
    return {
      request: function (request, config) {
        return request;
      },
      response: function (response) {

        if (_.has(response.data, 'data')) {
          response.data = response.data.data;
        }
        
        return response;
      }
    }

  })

}(
  typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require) }
  // Boilerplate for AMD and Node
))
