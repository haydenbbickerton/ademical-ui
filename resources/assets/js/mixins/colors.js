(function () {

  var Color = require("color");

  var vue; // lazy bind

  var baseColors = {
            lightBlue: "#3c8dbc",
            red: "#f56954",
            green: "#00a65a",
            aqua: "#00c0ef",
            yellow: "#f39c12",
            blue: "#0073b7",
            navy: "#001F3F",
            teal: "#39CCCC",
            olive: "#3D9970",
            lime: "#01FF70",
            orange: "#FF851B",
            fuchsia: "#F012BE",
            purple: "#8E24AA",
            maroon: "#D81B60",
            black: "#222222",
            gray: "#d2d6de",
            clear: "rgba(0,0,0,0)"
    }

    /**
     *
     *  Main Colors
     * 
     */
    
    // Blank object, this will hold our colors
    var colors = {
        active: {},
        disabled: {},
        charts: {}
    };

    // Loop through every base color, create the states(active, disabled),
    // and push it to our color array
    _.forEach(baseColors, function(hex, key) {
        var color = Color(hex);

        colors[key] = color.rgbString();
        colors.active[key] = color.clone().darken(0.25).rgbString();
        colors.disabled[key] = color.clone().lighten(0.25).rgbString();
    });




    /**
     *
     *  Chart Colors
     * 
     */
    
    // Our chart colors are just aliases for some real colors
    var statColorMap = {
        clicks: 'blue',
        ctr: 'aqua',
        cost: 'green',
        conversions: 'purple',
        avg_cpc: 'teal',
        impressions: 'yellow',
        avg_position: 'orange'
    };


    // Blank array to hold stat colors
    var statColors = {
        color: {},
        background: {},
        border: {},
        pointerBackground: {},
        pointerBorder: {},
    };

    // Similar to above, except we'll reference the baseColor array using
    // the statColorMap for our colors
    _.forEach(statColorMap, function(colorName, key) {
        var hex = baseColors[colorName];
        var color = Color(hex);

        statColors.color[key] = color.clone().rgbString();
        statColors.background[key] = color.clone().clearer(0.85).rgbString();
        statColors.border[key] = color.clone().desaturate(0.5).lighten(0.4).clearer(0.15).rgbString();
        statColors.pointerBorder[key] = color.clone().desaturate(0.15).darken(0.6).rgbString();
        statColors.pointerBackground[key] = color.clone().desaturate(0.15).darken(0.3).rgbString();
    });

    colors.stats = statColors;

    /*
        Our Vue instance properties
     */
    var appColors = {
        data: function()
        {
            return {
                colors: colors
            }
        }
    }

  var api = {
    mixin: appColors,
    install: function (Vue, options) {
      vue = Vue;
      Vue.options = Vue.util.mergeOptions(Vue.options, appColors);
    }
  }

  if(typeof exports === 'object' && typeof module === 'object') {
    module.exports = api
  } else if(typeof define === 'function' && define.amd) {
    define(function () { return api })
  } else if (typeof window !== 'undefined') {
    window.appColors = api
  }
})()
