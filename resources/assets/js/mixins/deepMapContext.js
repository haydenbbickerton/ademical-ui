function deepMapContext(obj, context) {
    return _.transform(obj, function(result, val, key) {
    	if (_.isObject(val)) {
              result[key] = _.has(val,context) ?
                            deepMapContext(val.data, context) :
                            deepMapContext(val, context);
    	} else {
    		result[key] = val;
    	}
    });
}

_.mixin({
   deepMapContext: deepMapContext
});