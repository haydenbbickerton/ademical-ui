/*
 * HEY, LISTEN!
 *
 * The guest option doesn't say "Anybody can get to this page".
 * It says "ONLY guests can get here". 
 *
 * Ex - If you are logged in, you shouldn't access the login page; only
 *       guests (users not logged in) should be able to get there.
 */

module.exports = {

    configRouter: function(router)
    {

        router.map(
        {
            '/advertisement':
            {
                name: 'advertisement',
                component: require('./pages/advertisement.vue'),
                auth: true,
                subscribed: true,
                subRoutes:
                {
                    '/':
                    {
                        component: require('./pages/advertisement/index.vue')
                    }
                }
            },
            '/accounts':
            {
                name: 'accounts',
                component: require('./pages/accounts.vue'),
                auth: true,
                subscribed: true,
                subRoutes:
                {
                    '/':
                    {
                        label: 'Account Management',
                        component: require('./pages/accounts/accounts.vue')
                    }
                }
            },
            '/auth':
            {
                name: 'auth',
                component: require('./pages/auth.vue'),
                subRoutes:
                {
                    '/':
                    {
                        component: require('./pages/auth/login.vue'),
                        subscribed: false,
                        guest: true
                    },
                    '/subscribe':
                    {
                        name: 'auth.subscribe',
                        component: require('./pages/auth/subscribe.vue'),
                        auth: true
                    },
                    '/redirect':
                    {
                        name: 'auth.redirect',
                        component: require('./pages/auth/redirect.vue'),
                        guest: true
                    },
                    '/profile':
                    {
                        name: 'auth.profile',
                        component: require('./pages/auth/profile.vue'),
                        auth: true
                    },
                    '/logout':
                    {
                        name: 'auth.logout',
                        component: require('./pages/auth/logout.vue'),
                        auth: true
                    }
                }
            },
            '/terms':
            {
                component: require('./pages/terms.vue')
            },
            '*':
            {
                component: require('./pages/404.vue')
            }
        });

        router.redirect({
          // redirect any not-found route to home
          '*': '/advertisement'
        });

        router.beforeEach(function({ to, next, redirect })
        {
            /*
             * TODO: I don't know why or how, but I don't think I'm doing this very well.
             * I'm pretty sure I could better arrange these promises somehow. Just don't how yet.
             *
             * Note
             * ------------------------------------------------------------------------
             * I've probably spent like 15 hours trying to come up with a better way for this.
             * Data setup shouldn't be in the view routing file. I'm gonna put an hour count below,
             * I'll increment it every time I try to fix this code and fail.
             *
             * Hours: 15
             * Updated 1-20-2016
             */


            /*
             * If the kickoff hasn't been done yet, do it and return true when finished.
             * If it has been done just return true.
             */
            if (!appInstance.$get('kickedOff')) {
                var kickoff = appInstance.kickOff().then(function() { return true; });
            } else {
                var kickoff = true;
            }

            /*
             * Make sure the kickoff has been performed before we start doing things.
             */
            Promise.all([kickoff]).then(function() { 

                  if (to.auth)
                {
                    if (appInstance.$get('authenticated')) 
                    {
                        if (to.subscribed)
                        {
                            if (appInstance.$get('subscribed') !== true)
                            {
                                console.log('not subscribed, redirect.');
                                redirect('/auth/subscribe');
                            }
                        }
                    } else {
                        redirect('/auth');
                    }
                }

                if (to.guest)
                {
                    if (appInstance.$get('authenticated'))
                    {
                        // They are trying to go to a page that only
                        // guests can reach. Send them to home.
                        redirect('/home');
                    }
                }

                //console.log('transitioning next');
                next();

            });
       
        })
    }
}