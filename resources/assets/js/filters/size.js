/**
 * Return the size of passed value.
 *
 * @link https://lodash.com/docs#size
 */
exports.install = function (Vue, options) {
	Vue.filter('size', function(value) {
	  return _.size(value);
	});
};

