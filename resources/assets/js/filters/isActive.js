/**
 * Filter through the values, return the active ones.
 *
 */
exports.install = function (Vue, options) {
  Vue.filter('isActive', function (values) {
    return _.filter(values, {'active': true });
  });
};

