/*
	UI Components
 */
window.componentFooter = require('./ui/footer.vue');
window.componentHeader = require('./ui/header.vue');
window.componentSidebar = require('./ui/sidebar.vue');

	//Partials
	window.componentSpinner = require('./ui/partials/spinner.vue');
	window.componentCancelCheck = require('./ui/partials/cancelCheck.vue');

	//Boxes
	window.componentInfoBox = require('./ui/boxes/infoBox.vue');
	window.componentMathBox = require('./ui/boxes/mathBox.vue');

/*
	Chart Components
 */
window.componentChartOverview = require('./charts/overview.vue');

/*
	Stripe Components
 */
window.componentStripe = require('./stripe/stripe.vue');