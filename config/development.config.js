var path = require("path");

var config = {
    env: 'development',
    paths: {
        root: path.resolve(__dirname, '..'),
        get public () {
            return this.root + 'public/';
        },
        get assets () {
            return this.root + 'resources/assets/';
        },
        get charts () {
            return this.assets + 'js/components/charts/';
        }
    },
    http:
    {
        options:
        {
            root: 'http://api.ademical.dev'
        },
        headers:
        {
            common:
            {
                'Accept': 'application/x.ademical.v1+json',
                'Content-Type': 'application/json'
            }
        }

    },
    services:
    {
        google:
        {
            client_id: '353504329200-k1q8d63jh5mshld30goulcp0mikmlc27.apps.googleusercontent.com',
            client_secret: 'KkzKoYta8WYl3TQrCS31QjXM',
            redirect_uri: 'http://app.ademical.dev/auth/redirect',
            response_type: 'code',
            scope: ['https://www.googleapis.com/auth/adwords', 'email'],
            adwords:
            {
                developer_token: 'uyBoZ6ubH5BkwYINA3GbpQ',
                user_agent: 'Frank & Maven'
            }
        },
        stripe:
        {
            publishable_key: 'pk_test_i0HEjP2hmELQzHWrZkSOLG7Z'
        },
        pusher:
        {
            app_id: '157315',
            key: 'a7d0df8d993ba8cdf11e',
            secret: 'fb9bc932852de42854af',
            options:
            {
                encrypted: true
            } 
        }
    },
    debug: true
}
module.exports = config;