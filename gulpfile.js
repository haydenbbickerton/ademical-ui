// Disable notify
process.env.DISABLE_NOTIFIER = true

var elixir = require('laravel-elixir')

elixir.config.js.browserify.transformers.push({ name: 'envify' });

elixir.config.js.browserify.transformers.push({
    name: 'vueify',
    // https://github.com/vuejs/vueify#usage
    options: {}
});

elixir.config.js.browserify.transformers.push({ 
	name: 'browserify-shim', 
	options: {
	}
});

// Generate source map for easier debugging in dev tools
elixir.config.js.browserify.options.debug = true;

var paths = {
    'assets': 'resources/assets/',
    'adminlte': './node_modules/admin-lte/',
    'bootstrap': './node_modules/bootstrap/',
    'node': './node_modules/',
    'skeuocard': './node_modules/Skeuocard/',
    'motionui': './node_modules/motion-ui/'
}

elixir(function (mix) {
  	/*
  		This takes awhile and doesn't really change,
  		uncomment when needed.
	*/
	/*
		 I'm doing this by itself because it causes problems
		 when built in the same process as bootstrap. It shares some
		 variables that error out the build process.
	 */
	mix.less(paths.assets + 'less/vendor/AdminLTE.less', './public/css/AdminLTE.css');

    mix
  	.less([
        paths.assets + 'less/vendor/init.less'
    ], 'public/css/vendor.css')
    .styles([
        './public/css/vendor.css',
        './public/css/AdminLTE.css',
        paths.motionui +'dist/motion-ui.min.css'
    ], 'public/css/vendor.css')
	.copy(
		paths.node + 'hellojs/src', 'public/js/hello'
	)
	.copy(
		paths.node + 'font-awesome/fonts', 'public/fonts'
	)
	.copy(
		paths.bootstrap + 'fonts', 'public/fonts'
	)
	.scripts([
		paths.bootstrap + 'dist/js/bootstrap.min.js',
        paths.adminlte + 'dist/js/app.min.js'
	], 'public/js/vendor.js');

	
	
	mix
	.less([
        paths.assets + 'less/init.less'
    ], 'public/css/app.css')
    .browserify('bootstrap.js', 'public/js/app.js');

});